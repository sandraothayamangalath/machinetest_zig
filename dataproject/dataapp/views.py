from django.shortcuts import render,redirect,HttpResponseRedirect
from . models import *
from django.views import  View
from django.contrib.auth.hashers import  check_password
from django.http import JsonResponse,HttpResponse
from django.core import serializers

# Create your views here.



def grade(request):
        id_student=request.GET.get('id_s')
        postData = None
        if request.method=='POST':
            postData = request.POST
            subject = postData.get('subject')
            score = postData.get('score')
            tscore = postData.get('tscore')
            grade = postData.get('grade')
            eid= postData.get('pk',None)
            print(eid,".......................................")
            if eid != '0':
                editObj=Grades.objects.get(id=eid)
                editObj.student_id=id_student
                editObj.subject=subject
                editObj.mark=score
                editObj.in_mark=tscore
                editObj.grade=grade
                editObj.save()
            else:
                Grades(student_id=id_student,subject=subject,mark=score,in_mark=tscore,grade=grade).save()
                
        try:
            obj=Grades.objects.filter(student=id_student,is_deleted=False)
        except:
            obj=None
        return render(request,'grades.html',{"obj":obj})

def Editgrade(request):
        if request.method=='POST':
            id_grade=request.POST.get('id')
            id_sts=request.POST.get('sts')
            
            if id_sts =='0':
                dataobj=Grades.objects.filter(id=id_grade)
                data = serializers.serialize('json', dataobj)
                return HttpResponse(data, content_type="application/json")
            else:
                dataobj=Grades.objects.get(id=id_grade)
                dataobj.is_deleted=True
                dataobj.save()
                return JsonResponse({"msg":"success"})

def dashboard_admin(request):
          data=Student.get_all_students()
          return render(request,'dashboard_admin.html',{"data":data})

def profile(request):
          USERID=request.session['user']
          userobj=User.objects.get(id=USERID)
          return render(request,'profile.html',{"data":userobj})

def dashboard_student(request):
          sid=request.session['user']
          print(sid,"............................fff")
          try:
            data=Grades.objects.filter(student=sid,is_deleted=False)
          except:
              data=''
          return render(request,'dashboard_student.html',{"data":data})

def signup(request):
    if request . method =='POST':
        postData = request.POST
        fileobj=request.FILES.get('pro_pic')
        print(type(fileobj),"00000000000000000000000000000")
        uname = postData.get('uname')
        fname = postData.get('fname')
        lname = postData.get('lname')
        email = postData.get('email')
        dob = postData.get('dob')
        phone = postData.get('phone')
        password = postData.get('password')
        typeof = postData.get('hidden')
        if typeof == '0':
            Admin.objects.create_user(image=fileobj,username=uname,first_name=fname,last_name=lname,email=email,dob=dob,phone=phone,password=password)
        else:
            Student.objects.create_user(image=fileobj,username=uname,first_name=fname,last_name=lname,email=email,dob=dob,phone=phone,password=password)
        return redirect('login')
    return render(request,'signup.html')

class Login(View):
    return_url = None
    def get(self , request):
        Login.return_url = request.GET.get('return_url')
        return render(request , 'login.html')

    def post(self , request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = User.get_user_by_username(username)
        error_message = None
        if user:
            flag = check_password(password, user.password)
            if flag:
                request.session['user'] = user.id
                request.session['username'] = username
                request.session['user_type'] = user.user_type

                if Login.return_url:
                    return HttpResponseRedirect(Login.return_url)
                else:
                    Login.return_url = None
                    redirect_url=dashboard_admin if user.user_type=='Admin' else dashboard_student
                    return redirect(redirect_url)
            else:
                error_message = 'Username or Password invalid !!'
        else:
            error_message = 'Username or Password invalid !!'

        return render(request, 'login.html', {'error': error_message})

def logout(request):
    request.session.clear()
    return redirect('login')

def editprofile(request):
    fname=request.GET.get('firstname')
    lastname=request.GET.get('lastname')
    email=request.GET.get('email')
    phone=request.GET.get('phone')
    dob=request.GET.get('dob')
    editid=request.GET.get('id')
    print(fname,"............................jjjjjjj..............")
    Student.objects.filter(id=int(editid)).update(email=email,phone=phone,first_name=fname,last_name=lastname,dob=dob)
    return JsonResponse({"data":"data"})

def change_img(request):
    if request.method=='POST':
        img=request.FILES.get('file1')
        editid=request.POST.get('edit_id')
        print(type(img),".................>>>>>>>>>.")
        Student.objects.filter(id=editid).update(image=img)
        
        # objimg=Student.objects.get(id=editid)
        # objimg.image=img
        # objimg.save()
        return redirect('profile')
