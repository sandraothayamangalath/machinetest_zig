from django.urls import path,include
from .import views 


urlpatterns = [
    path('',views.signup, name='signup'),
    path('grade',views.grade),
    path('login', views.Login.as_view(), name='login'),
    path('logout', views.logout , name='logout'),
    path('profile', views.profile , name='profile'),
    path('editprofile',views.editprofile,name='editprofile'),
    path('home_student', views.dashboard_student , name='dashboard_student'),
    path('home_admin', views.dashboard_admin , name='dashboard_admin'),
    path('edit-grade', views.Editgrade , name='edit-grade'),
    path('change_img', views.change_img , name='change_img'),
    
]