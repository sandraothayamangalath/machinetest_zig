from django import template
from datetime import datetime


register = template.Library()

@register.filter(name='change_date')
def change_date(date):
    print(type(date),"...............////////")
    date_object = datetime.strptime(str(date), "%Y ,%B %d")
    return date_object


