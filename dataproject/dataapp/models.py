from datetime import datetime
from distutils.command.upload import upload
from re import T
from urllib import request
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    class UserType(models.TextChoices):
        SUPERADMIN = "Superadmin", "superadmin"
        ADMIN = "Admin", "admin"
        STUDENT = "Student", "student"

    base_user_type = UserType.SUPERADMIN 
    user_type = models.CharField(max_length=20, choices=UserType.choices)
    dob = models.DateField(null=True,blank=True)
    phone = models.CharField(max_length=20,null=True,blank=True)
    image = models.ImageField(upload_to='uploads/products/')

    def save(self, *args, **kwarg):
        if not self.pk:  # only in create not in edit
            self.user_type = self.base_user_type
            return super().save(*args, **kwarg)

    @staticmethod
    def get_user_by_username(username):
        try:
            return User.objects.get(username=username)
        except:
            return False

class Admin(User):
    base_user_type = User.UserType.ADMIN

    class meta:
        proxy = True


class Student(User):
    base_user_type = User.UserType.STUDENT

    class meta:
        proxy = True

    @property
    def age(self):
        return int((datetime.now().date()-self.dob).days/365.25)
    
    @staticmethod
    def get_all_students():
        return Student.objects.all()

    @staticmethod
    def get_students_id(id):
        return Student.objects.get(id=id)

class Grades(models.Model):
    student=models.ForeignKey(Student,on_delete=models.CASCADE,null=True,blank=True)
    subject=models.CharField(max_length=20,null=True,blank=True)
    mark=models.FloatField()
    in_mark=models.IntegerField()
    grade=models.CharField(max_length=20,null=True,blank=True)
    is_deleted=models.BooleanField(default=False)
    created_on=models.DateField(auto_now_add=True,null=True,blank=True)
    updated_on=models.DateField(auto_now=True,null=True,blank=True)


