from django.shortcuts import redirect

def auth_middleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        print(request.path,".............................oooooooooooooo")
        returnUrl = request.META['PATH_INFO']
        if not request.session.get('user') and (request.path != '/' or request.path != '/login'):
           return redirect('login')

        response = get_response(request)
        return response

    return middleware